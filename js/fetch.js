window.onload = function () {
    const container = document.getElementById("film-cont");
    const planetList = fetch("https://swapi.co/api/planets/");
    planetList.then(result => result.json())
        .then((res) => {
            return res.results.map(elem => {
                let newDiv = document.createElement("div");
                newDiv.className = "film-item";
                newDiv.id = `${elem.name.replace(/ /g, '')}`;
                if (elem.residents.length > 0) {
                    newDiv.setAttribute('person', elem.residents);
                }
                newDiv.innerHTML = `<p class="item" id=name-${elem.name.replace(/ /g, '')}>Имя: ${elem.name}</p>
                <p class="item">Климат: ${elem.climate}</p><p class="item">Местность: ${elem.terrain}</p>`;
                container.appendChild(newDiv);
                return newDiv;
            });
        }).then((result) => {
            result.forEach(element => {
                let id = element.id;
                let strPerson = ``;
                let arr = [];
                if (element.getAttribute('person')) {
                    arr = element.getAttribute('person').split(",");
                }
                if (arr.length > 0) {
                    let newP = document.createElement('p');
                    arr.forEach((el) => {
                        Promise.all([
                            fetch(el)
                            .then(result => result.json())
                            .then(person => {
                                strPerson += `${person.name}, `;
                                return strPerson;
                            }).then(list => {
                                newP.innerHTML = `Персонажи: ${list}`;
                                document.getElementById(id).appendChild(newP);
                            })
                        ]);
                    });
                }
            });
        });
}    